*** Settings ***
Library           SSHLibrary

*** Test Cases ***
Check VM
    Connect And Check Directory    192.168.33.10    vagrant    vagrant    ls -alt    exercise_7.2

*** Keywords ***
Connect And Check Directory
    [Arguments]    ${host}    ${user}    ${password}    ${command}    ${name}
    Open Connection    ${host}
    Login    ${user}    ${password}
    ${result}    Execute Command    ${command}
    Log    ${result}
    Should Contain    ${result}    ${name}